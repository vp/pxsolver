all: compile test

compile:
	@cd build/ && sudo make install 
test:
	@python scripts/img2dispenser.py images/tracking6.jpg && pxsolver dispenser.txt potions.txt
bot:
	@cd scripts && python bot.py
