#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.

import logging
import os
import subprocess
import random
import configparser
import string
from captcha.image import ImageCaptcha
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)
from img2dispenser import img2dispenser
from emoji import emojize
import csv

parser = configparser.ConfigParser()
parser.read('config.ini')
REPLY = range(1)
captcha_maps = dict()
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Message format is potion | potion
# each potion is described by <colored_hearts> <points>
def potions_parser(string):
	first = { 'R' : 0, 'C' : 0, 'Y' : 0, 'B' : 0 }
	first_points = 0
	second = { 'R' : 0, 'C' : 0, 'Y' : 0, 'B' : 0 }
	second_points = 0
	red = "❤️💛💙🖤"
	potions = string.split("|")
	first['R'] = potions[0].count(emojize(":red_circle:"))
	first['Y'] = potions[0].count(emojize(":yellow_circle:"))
	first['C'] = potions[0].count(emojize(":blue_circle:"))
	first['B'] = potions[0].count(emojize(":black_circle:"))
	second['R'] = potions[1].count(emojize(":red_circle:"))
	second['Y'] = potions[1].count(emojize(":yellow_circle:"))
	second['C'] = potions[1].count(emojize(":blue_circle:"))
	second['B'] = potions[1].count(emojize(":black_circle:"))
	first_points = potions[0].strip().split(" ")[-1]
	second_points = potions[1].strip().split(" ")[-1]
	print(first)
	print(second)
	with open("potions.txt","w") as f:
		f.write("4 " + first_points + "\n")
		for item in  first.items():
			f.write(item[0] + " " + str(item[1]) + "\n")
		f.write("\n")
		f.write("4 " + second_points + "\n")
		for item in  second.items():
			f.write(item[0] + " " + str(item[1]) + "\n")
	pass

def get_instructions():
	with open("instructions.txt","r") as f:
		return f.read(-1)

def parse_dispenser():
	dispenser = "Dispenser"
	with open("stats.csv","r") as f:
		csv_file = csv.reader(f)
		for row in csv_file:
			i = int(row[0])
			j = int(row[1])
			ingr = str(row[2])
			if j == 0:
				dispenser += '\n'
			if ingr == 'R':
				dispenser += emojize(':red_circle:')
			elif ingr == 'Y':
				dispenser += emojize(':yellow_circle:')
			elif ingr == 'C':
				dispenser += emojize(':blue_circle:')
			elif ingr == 'B':
				dispenser += emojize(':black_circle:')
			else:
				dispenser += emojize(':brown_circle:')
	return dispenser

def parse_fit_score():
	highest_fit = 0.0
	x = 0
	y = 0
	with open("stats.csv","r") as f:
		csv_file = csv.reader(f)
		for row in csv_file:
			if float(row[4]) > highest_fit:
				highest_fit = float(row[4])
				x = int(row[0])
				y = int(row[1])
		final =  "You get " + str(highest_fit*100)
		final += "% of your needs at: "
		final += "(" + str(x + 1) + "," + str(y + 1) + ")"
		return final

def get_legenda():
	legenda = "Legenda\n"
	disclaimer = "Please notice that the dispenser reconstruction might be"\
		" wrong and so the instructions, try to take another picture"\
		" showing clearly the"\
		" top view of the dispenser and avoid shadows.\n"
	legenda += disclaimer
	balls = emojize(':red_circle:') + ": Red ingredient\n"
	balls += emojize(':yellow_circle:') + ": Yellow ingredient\n"
	balls += emojize(':blue_circle:') + ": Blue ingredient\n"
	balls += emojize(':black_circle:') + ": Black ingredient\n"
	balls += emojize(':brown_circle:') + ": Empty point\n"
	
	help = "The scores, are the minimum affordable scores, so"\
		" it is possible that luckily enough you will get"\
		" more points, if you trigger explosions with"\
		" ingredients hidden by the top part of the dispenser"
	
	legenda += balls + help
	
	return legenda

def get_potions_format():
	help = "Potions format\n"
	help += "Potions must be formatted as in this example"\
		"For a potion of 4 points made by 2 blue and 2 blacks"\
		"and another of 6 points made by 3 red and 2 yellow:\n"
	help += emojize(':blue_circle:') + emojize(':blue_circle:') +\
		emojize(':black_circle:') + emojize(':black_circle:') +\
		" 4 | "
	help += emojize(':red_circle:') + emojize(':red_circle:') +\
		emojize(':red_circle:') + emojize(':yellow_circle:') +\
		emojize(':yellow_circle:') + " 6\n"
	help += "Note that order of ingredients is not important, but it"\
		" is important that you leave a space between the last"\
		" ingredient and the points"
	
	return help

def ping(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Pong')

def help(update: Update, context: CallbackContext) -> None:
	help_msg = "Help:\n"
	help_msg += get_legenda() +"\n\n"+ get_potions_format()
	help_msg += "To start calculating send a picture of the top"\
		" view of the dispenser without shadows and in the caption"\
		" write your ingredients as specified in the format"
	update.message.reply_text(help_msg)
    
def picture(update: Update, context: CallbackContext) -> None:
	update.message.reply_text('Picture received, starting to work...')
	potions = str(update.message.caption)
	update.message.photo[0].get_file().download(custom_path="image.jpg")
	img2dispenser()
	# creating the potions file
	potions_parser(potions)
	# executing pxsolver
	os.system("pxsolver dispenser.txt potions.txt")
	# gathering instructions
	final_stats = "Final Statistics\n"
	instructions = get_instructions()
	final_stats += instructions + "\n"
	# reading statistics and csv
	dispenser = parse_dispenser()
	final_stats += dispenser + "\n"
	fit_score = parse_fit_score()
	final_stats += fit_score + "\n"
	legenda = get_legenda()
	final_stats += legenda
	update.message.reply_text(final_stats)


def dispenser(update: Update, context: CallbackContext) -> None:
	disp = str(update.message.text).split("-")[0].strip()
	with open("dispenser.txt","w") as f:
		for char in disp:
			if char == emojize(':red_circle:'):
				f.write('R')
			elif char == emojize(':yellow_circle:'):
				f.write('Y')
			elif char == emojize(':blue_circle:'):
				f.write('C')
			elif char == emojize(':black_circle:'):
				f.write('B')
			elif char == '\n':
				f.write('\n')
	potions = str(update.message.text).split("-")[1].strip()
	# creating the potions file
	potions_parser(potions)
	# executing pxsolver
	os.system("pxsolver dispenser.txt potions.txt")
	# gathering instructions
	final_stats = "Final Statistics\n"
	instructions = get_instructions()
	final_stats += instructions + "\n"
	# reading statistics and csv
	dispenser = parse_dispenser()
	final_stats += dispenser + "\n"
	fit_score = parse_fit_score()
	final_stats += fit_score + "\n"
	legenda = get_legenda()
	final_stats += legenda
	update.message.reply_text(final_stats)
	pass

def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    tok = str(parser["Settings"]["Token"])
    updater = Updater(token=str(tok), use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("ping", ping))
    dispatcher.add_handler(CommandHandler("help", help))
    dispatcher.add_handler(ConversationHandler(
        entry_points=[MessageHandler(Filters._Photo(),picture)],
        states={
        },
        fallbacks=[None],per_chat=True,per_user=False
    ))
    dispatcher.add_handler(ConversationHandler(
        entry_points=[MessageHandler(Filters._All(),dispenser)],
        states={
        },
        fallbacks=[None],per_chat=True,per_user=False
    ))
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
