import sys
import numpy as np
import cv2
import numpy as np

ROWS = 8
COLS = 5
MAX_X = -1
MAX_Y = -1
IMAGE_NAME = "image.jpg"
dispenser = np.zeros((ROWS,COLS),dtype=str)

def update_max(x,y):
    global MAX_X
    global MAX_Y
    if (x > MAX_X):
        MAX_X = x
    if (y > MAX_Y):
        MAX_Y = y

def reset_max():
    global MAX_X
    global MAX_Y
    MAX_X = -1
    MAX_Y = -1

def add_element(x,y,colour):
    global MAX_X
    global MAX_Y
    step_y = float(MAX_Y/ROWS)
    step_x = float(MAX_X/COLS)
    i = 1
    j = 1
    while (x > j*step_x):
        j += 1
    while (y > i*step_y):
        i += 1
    dispenser[i-1][j-1] = colour

def fill_dispenser(image, colour):
    global MAX_X
    global MAX_Y
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Find circles
    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 50,
              param1=30,
              param2=15,
              minRadius=1,
              maxRadius=25)
    # If some circle is found
    if circles is not None:
        # Get the (x, y, r) as integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the circles
        for (x, y, r) in circles:
           update_max(x,y)
        for (x, y, r) in circles:
           add_element(x,y,colour)
    reset_max()

def track_ingredients(image):
    output = image.copy()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Find circles
    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1, 50,
              param1=30,
              param2=15,
              minRadius=1,
              maxRadius=25)
    # If some circle is found
    if circles is not None:
       # Get the (x, y, r) as integers
       circles = np.round(circles[0, :]).astype("int")
       # loop over the circles
       for (x, y, r) in circles:
          cv2.circle(output, (x, y), r, (0, 255, 0), 2)
        
    # show the output image
    #cv2.imshow("circle",output)
    #cv2.waitKey(0)
    return


def write_dispenser():
    global dispenser
    allowed_colour = ['R', 'C', 'Y', 'B']
    #before printing replacing empty values
    i = 0
    j = 0
    for i in  range(0,len(dispenser)):
        for j in range(0,len(dispenser[i])):
            if dispenser[i][j] not in allowed_colour:
                dispenser[i][j] = 'X'
                

    with open("dispenser.txt","w") as f:
        for line in dispenser:
            f.writelines(line)
            f.write("\n")


def create_mask():
    global IMAGE_NAME
    RED = np.uint8([[[36, 28, 237]]])
    YELLOW = np.uint8([[[2, 188, 201]]])
    CYAN = np.uint8([[[194, 96, 1]]])
    WHITE = np.uint8([[[255,255,255]]])
    hsv_colors = []
    hsv_colors.append(cv2.cvtColor(RED, cv2.COLOR_BGR2HSV))
    hsv_colors.append(cv2.cvtColor(YELLOW, cv2.COLOR_BGR2HSV))
    hsv_colors.append(cv2.cvtColor(CYAN, cv2.COLOR_BGR2HSV))
    img = cv2.imread(IMAGE_NAME, 1)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    masks = []
    for hsv_color in hsv_colors:
        hue = hsv_color[0][0][0]  # Getting hue value
        lower_range = np.array([hue - 5, 100, 100], dtype=np.uint8)
        upper_range = np.array([hue + 5, 255, 255], dtype=np.uint8)
        masks.append(cv2.inRange(hsv, lower_range, upper_range))

    #BLACK
    invert = cv2.bitwise_not(img)
    hsv = cv2.cvtColor(invert, cv2.COLOR_BGR2HSV)
    hsv_color = cv2.cvtColor(WHITE, cv2.COLOR_BGR2HSV)
    min_col = 210
    lower_range = np.array([min_col,min_col,min_col], dtype=np.uint8)
    upper_range = np.array([255,255,255], dtype=np.uint8)
    white_mask = cv2.inRange(invert, lower_range, upper_range)
    white_output =  cv2.bitwise_and(invert, invert, mask=white_mask)
    # END BLACK

    outputs = []
    for mask in masks:
        outputs.append(cv2.bitwise_and(img, img, mask=mask))

    result = outputs[0] + outputs[1] + outputs[2] + white_output
    track_ingredients(white_output)
    for output in outputs:
        track_ingredients(output)
    # filling dispenser
    fill_dispenser(white_output,'B')
    fill_dispenser(outputs[0], 'R')
    fill_dispenser(outputs[1],'Y')
    fill_dispenser(outputs[2],'C')

    cv2.waitKey()

    cv2.destroyAllWindows()

def img2dispenser():
    create_mask()
    write_dispenser()
