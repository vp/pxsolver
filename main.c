#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "constants.h"
#include "potion.h"


void readDispenser(char [ROWS][COLS], char *);
void readPotions(potion_t *, potion_t *, char *);
void calculate(char [ROWS][COLS],
	       int[ROWS][COLS],
	       float [ROWS][COLS],
	       potion_t, 
		potion_t);
int  score(char *pool,int n, potion_t first, potion_t second);
int take_ingredient(char pool[ROWS],
		    char dispenser[ROWS][COLS],
		    int i,
		    int j);

char help_professor(char dispenser_copy[ROWS][COLS],
		    int i,
		    int j,
		    char drop_colour);
void copy_dispenser(char src[ROWS][COLS], char dest[ROWS][COLS]);
char probable_drop(char dispenser[ROWS][COLS]);
int max(int array[ROWS], int *pos);
void print_instructions(FILE *fp,int score,int instr,...);
float fit_score(char *pool,int n, potion_t first, potion_t second);
void write_stats(char dispenser[ROWS][COLS],
		 int scores[ROWS][COLS],
		 float fit_scores[ROWS][COLS]);

int main(int argc, char *argv[])
{
	char dispenser[ROWS][COLS];
	int scores[ROWS][COLS] = {{ 0 }};
	float fit_scores[ROWS][COLS] = {{ 0.0 }};

	potion_t first,second;
	int i,j;
	
	if (argc < 3) {
		perror(MISSING_PARAMS);
		exit(EXIT_FAILURE);
	}

	readDispenser(dispenser,argv[1]);
	readPotions(&first, &second, argv[2]);
	PRETTY_PRINT("Dispenser");
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			SWITCH_COLOR(dispenser[i][j]);
			printf(" %c ",dispenser[i][j]);
			SWITCH_COLOR('X');
		}
		printf("\n");
	}
	calculate(dispenser,scores,fit_scores,first,second);
	PRETTY_PRINT("Scores");
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			printf(" %d ",scores[i][j]);
		}
		printf("\n");
	}
	PRETTY_PRINT("Fit scores");
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			printf(" %.1f ",fit_scores[i][j]);
		}
		printf("\n");
	}
	PRETTY_PRINT("Potions");
	printPotion(first);
	printf("\n\n");
	printPotion(second);
	write_stats(dispenser,scores,fit_scores);
	
	exit(EXIT_SUCCESS);
}


void readDispenser(char matrix[ROWS][COLS], char *filename)
{
	FILE *fp = fopen(filename,"r");
	int i,j;
	
	/*Reading from file*/
	
	if (!fp) {
		perror("Error during read of file!");
		exit(EXIT_FAILURE);
	}
	
	
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			fscanf(fp," %c ",&(matrix[i][j]));
		}
	}
}

void readPotions(potion_t * first, potion_t * second, char * filename)
{
	FILE *fp = fopen(filename, "r");
	int n, points;
	char color;
	int i;

	if (!fp) {
		perror("Error during reading of potion file");
		exit(EXIT_FAILURE);
	}
	
	fscanf(fp, " %d %d", &n, &(first->points));
	
	for (i = 0; i < TOT_COLORS; i++) {
		first->colors[i] = 0;
		second->colors[i] = 0;
	}

	for (i = 0; i < n; i++) {
		fscanf(fp, " %c %d",&color, &points);
		
		switch (color) {
			case 'R':
				first->colors[_RED] = points;
				break;
			case 'Y':
				first->colors[_YELLOW] = points;
				break;
			case 'C':
				first->colors[_CYAN] = points;
				break;
			case 'B':
				first->colors[_BLACK] = points;
				break;
			default:
				break;
		}
	}
	
	fscanf(fp, " %d %d", &n, &(second->points));

	for (i = 0; i < n; i++) {
		fscanf(fp, " %c %d",&color, &points);
		
		switch (color) {
			case 'R':
				second->colors[_RED] = points;
				break;
			case 'Y':
				second->colors[_YELLOW] = points;
				break;
			case 'C':
				second->colors[_CYAN] = points;
				break;
			case 'B':
				second->colors[_BLACK] = points;
				break;
			default:
				break;
		}
	}
}


void calculate(char dispenser[ROWS][COLS],
	       int scores[ROWS][COLS],
	       float fit_scores[ROWS][COLS],
	       potion_t first,
	       potion_t second)
{
	int i,j,tests[ROWS],k;
	char pool[ROWS]; /*Maximum ingredients that can be taken*/
	int n;
	char dispenser_copy[ROWS][COLS];
	int max_score = 0;
	int pos;

	for ( j = 0; j < COLS; j++) {
		for (i = 1; i < ROWS - 1; i++) {
			n = 0;
			max_score = 0;
			/*Resetting dispenser at each iteration*/
			copy_dispenser(dispenser,dispenser_copy);
			if (dispenser_copy[i][j] == EMPTY)
				continue;
			/*
			 * Simulating without help of professor
			 */
			n = take_ingredient(pool,dispenser_copy,i,j);
			tests[0] = score(pool,n,first,second);
			fit_scores[i][j] = fit_score(pool,n,first,second);
			/*Calculating probabilistic drop if
			 * we get this (i,j) ingredient with
			 * help of professor
			 */
			char drop = probable_drop(dispenser);
			/**/
			/*Simulating help of professor on each
			 * ingredient of the column of the dispenser
			 */
			for (k = 1; k < ROWS; k++) {
				copy_dispenser(dispenser,dispenser_copy);
				help_professor(dispenser_copy,k,j,drop);
				/*Now taking ingredient after help of
				 * professor
				 */
				n = take_ingredient(pool,dispenser_copy,i,j);
				/*Removing points from the professor*/
				tests[k] = score(pool,n,first,second) - 2;
			}
			max_score = max(tests,&pos);
			if (max_score) {
				/*Printing instructions to reach that score*/
				if (pos) {
					char instr1[200];
					sprintf(instr1,"Use help of "
						"professor to get ingredient "
						"at position (%d,%d)",
						pos+1,j+1
					);
					char instr2[200];
					sprintf(instr2,"Take ingredient from "
						"position (%d,%d)",
						i+1,j+1
					);
					print_instructions(stdout,
							   max_score,
							   2,
							   instr1,
							   instr2);
					print_instructions(NULL,
							   max_score,
							   2,
							   instr1,
							   instr2);
				} else {
					/*If pos is 0, then no help of
					 * professor was used
					*/
					char instr[200];
					sprintf(instr,"Take ingredient from "
						"position (%d,%d)",
						i+1,j+1
					);
					print_instructions(stdout,
							   max_score,
							   1,
							   instr);
					print_instructions(NULL,
							   max_score,
							   1,
							   instr);
				}
				
			}
			scores[i][j] = max_score;
		}
	}
	return;
}

int  score(char *pool,int n, potion_t first, potion_t second)
{
	int i;
	int red1,yellow1,cyan1,black1;
	int red,yellow,cyan,black;
	int red2,yellow2,cyan2,black2;
	red1 =yellow1 = cyan1 = black1 = 0;
	red2 =yellow2 = cyan2 = black2 = 0;
	red =yellow = cyan = black = 0;
	int can_do_first,can_do_second,can_do_both;
	can_do_first = can_do_second = can_do_both = 0;
	for (i = 0; i < n; i++) {
		switch (pool[i]) {
			case RED:
				red++;
				red1++;
				red2++;
				break;
			case YELLOW:
				yellow++;
				yellow1++;
				yellow2++;
				break;
			case CYAN:
				cyan++;
				cyan1++;
				cyan2++;
				break;
			case BLACK:
				black++;
				black1++;
				black2++;
				break;
			default:
				break;
		}
	}
	
	red1 -= first.colors[_RED];
	yellow1 -= first.colors[_YELLOW];
	cyan1 -= first.colors[_CYAN];
	black1 -= first.colors[_BLACK];
	red2 -= second.colors[_RED];
	yellow2 -= second.colors[_YELLOW];
	cyan2 -= second.colors[_CYAN];
	black2 -= second.colors[_BLACK];

	red -= first.colors[_RED] + second.colors[_RED];
	yellow -= first.colors[_YELLOW] + second.colors[_YELLOW];
	cyan -= first.colors[_CYAN] + second.colors[_CYAN];
	black -= first.colors[_BLACK] + second.colors[_BLACK];

	
	if (red1 >= 0 && yellow1 >= 0 && cyan1 >= 0 && black1 >= 0)
		can_do_first = 1;
	if (red2 >= 0 && yellow2 >= 0 && cyan2 >= 0 && black2 >= 0)
		can_do_second = 1;
	if (red >= 0 && yellow >= 0 && cyan >= 0 && black >= 0)
		can_do_both = 1;

	
	if (can_do_both) {
		return first.points + second.points;
	} else if (can_do_first && can_do_second) {
		if (first.points > second.points) {
			return first.points;
		} else
			return second.points;
	} else if (can_do_first)
		return first.points;
	else if (can_do_second)
		return second.points;
	
	return 0;
}

int take_ingredient(char pool[ROWS],
		    char dispenser[ROWS][COLS],
		    int i,
		    int j
				) {
	/*Simulating explosion of ingredients*/
	int up,down;
	up = down = 1;
	int mismatch = 0;
	int n = 0;
	pool[0] = dispenser[i][j];
	n++;
	while (!mismatch) {
		if (i-up <0 || i+down >= ROWS) {
			break;
		}
		if (dispenser[i-up][j] ==
			dispenser[i+down][j]) {
			pool[n] = dispenser[i-up][j];
			pool[n+1] = dispenser[i+down][j];
			n += 2;
			up++;
			while (dispenser[i-up][j] ==
				dispenser[i+down][j] &&
			i - up >= 0) {

				pool[n] = dispenser[i-up][j];
				n++;
				up++;
			}
			down++;
			while (dispenser[i+down][j] ==
				dispenser[i-up+1][j]
				&& i + down < ROWS) {
				pool[n] = dispenser[i+down][j];
				n++;
				down++;
			}
		} else
			mismatch = 1;
	}
	return n;
}


void copy_dispenser(char src[ROWS][COLS], char dest[ROWS][COLS]) {
	
	int i,j;
	
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			dest[i][j] = src[i][j];
		}
	}
}

char help_professor(char dispenser_copy[ROWS][COLS],
		    int i,
		    int j,
		    char drop_colour) {
	/*Simulating what would be the new matrix if we
	 * take an ingredient and a new drop_colour appears.
	 * When removing at position i, ingredients gets shifted
	 * down. This is simulated by array shift.
	 */
	char removed = dispenser_copy[i][j];
	while (i > 0) {
		dispenser_copy[i][j] = dispenser_copy[i-1][j];
		i--;
	}
	
	dispenser_copy[i][j] = drop_colour;
	
	return removed;
}

char probable_drop(char dispenser[ROWS][COLS]) {
	/* Returns most probable colour, basing on
	 * colors that can be seen on dispenser,
	 * as the less colour in numbers.
	 */
	
	int i,j;
	int counts[TOT_COLORS] = { 0 };
	int min = 3*ROWS;
	
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			switch(dispenser[i][j]) {
				case RED:
					counts[_RED]++;
					break;
				case YELLOW:
					counts[_YELLOW]++;
					break;
				case CYAN:
					counts[_CYAN]++;
					break;
				case BLACK:
					counts[_BLACK]++;
					break;
				default:
					break;
			}
		}
	}
	
	for (i = 0; i < TOT_COLORS; i++) {
		if (counts[i] < min) {
			min = counts[i];
			j = i;
		}
	}
	
	switch(j) {
		case _RED:
			return RED;
		case _YELLOW:
			return YELLOW;
		case _CYAN:
			return CYAN;
		case _BLACK:
			return BLACK;
		default:
			break;
	}
	
	return 0;
}

int max(int array[ROWS], int *pos) {
	int i;
	int max = -1;
	for (i = 0; i < ROWS; i++) {
		if (array[i] > max) {
			max = array[i];
			*pos = i;
		}
	}
	
	return max;
}

void print_instructions(FILE *fp,int score,int instr,...) {
	char title[100];
	va_list ap;
	int i;
	static int first_time = 0;
	
	if (!fp) {
		if (first_time == 0) {
			fp = fopen(INSTNAME,"w");
			first_time = 1;
		} else
			fp = fopen(INSTNAME,"a");
			
		if (!fp) {
			perror("Error while opening file");
			exit(EXIT_FAILURE);
		}
	}
	
	fprintf(fp,"Instructions %dpt\n",score);
	va_start(ap,instr);
	
	for (i = 0; i < instr; i++) {
		fprintf(fp,"%d) %s\n",i+1,va_arg(ap,char *));
	}
	
	if (fp != stdout)
		fclose(fp);
	
	va_end(ap);
}

float fit_score(char *pool,int n, potion_t first, potion_t second)
{
	int i;
	int red,yellow,cyan,black;
	red =yellow = cyan = black = 0;
	int total_needs[TOT_COLORS] = { 0 };
	int pool_colors[TOT_COLORS] = { 0 };
	float remaining_to_cover = 0;
	float total_to_cover = 0;
	
	for (i = 0; i < TOT_COLORS; i++) {
		total_to_cover += total_needs[i] += first.colors[i] + 
							second.colors[i];
	}

	for (i = 0; i < n; i++) {
		/*
		 * Only those ingredients needed get counted
		 * in the pool
		 */
		switch (pool[i]) {
			case RED:
				pool_colors[_RED]++;
				break;
			case YELLOW:
				pool_colors[_YELLOW]++;
				break;
			case CYAN:
				pool_colors[_CYAN]++;
				break;
			case BLACK:
				pool_colors[_BLACK]++;
				break;
			default:
				break;
		}
	}

	
	for (i = 0; i < TOT_COLORS; i++) {
		total_needs[i] -= pool_colors[i];
		if (total_needs[i] <= 0)
			continue;
		remaining_to_cover += total_needs[i];
	}
	
	return (1 - remaining_to_cover/total_to_cover);
}

void write_stats(char dispenser[ROWS][COLS],
		 int scores[ROWS][COLS],
		 float fit_scores[ROWS][COLS]) {
	int i,j;
	FILE *fp = fopen(STATSNAME, "w");
	
	if (!fp) {
		perror("Error during opening of stats file");
		exit(EXIT_FAILURE);
	}
	
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			fprintf(fp,"%d,%d,%c,%d,%.1f\n",
				i,j,
				dispenser[i][j],
				scores[i][j],
				fit_scores[i][j]);
		}
	}
	
	fclose(fp);
}
