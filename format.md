## Dispenser
Each ingredient is R(Red), Y(Yellow), C(Cyan), B(Black), X(Empty). The dispenser must be in the format:
```
RYCBB
YYYYY
XBCYY
.etc.
```

## Potions
Each potion must be represented with:
```
<n_ingredients > <n_points>
<color_char> <n_dots>
```
For example:
```
2 10
R 2
B 2

3 20
R 3
C 2
B 2
```
