# Pxsolver (Potion explosion solver)
Pxsolver is a program that manages to find best solutions for ingredient picking in potion explosion.
Simple demo:
![Image](images/info_output.png)

## Downloading and compiling
The software is rather light, so its compilation will take a pair of seconds. To download clone the repository or download it as a zip file.
After cloning/downloading it move into the directory and compile with:
``` cd pxsolver && make compile```

## How to use pxsolver
In order to use the software, read the ```format.md``` file, and the proceed using the software as:
```./pxsolver dispenser.txt potions.txt```
It will then output instructions that explain for each point the actions that 
can be made by the user.
## Computer Vision
In order to provide better usability from the end user, a computer vision script was built. The python script makes use of cv2 module and numpy as dependencies. To use it go in the scripts folder and run:
```
cd pxsolver/
python scripts/img2dispenser.py images/tracking6.jpg
pxsolver dispenser.txt potions.txt
```
You can provide also your picture to the computer vision.

## Bot
A telegram bot is under current development.

## What is missing?
Currently the algorithm for finding best solutions doesn't makes use potions 
that the player has crafted, which enormously limits the scores. The algorithm
doesn't try to best fit what's on the dispenser basing on requirements but tries
to get instantaneous points.

## Contributing
The software is fully open-source, feel free to:
* Open issues
* Open Pull requests
* Contact me for improvements on Telegram or via email at 
```vincenzo[dot]petrolo[at]protonmail[dot]com```.
