#ifndef __POTION_H__
#define __POTION_H__

#include <sys/types.h>
#include "constants.h"

typedef enum {
	_RED,
	_YELLOW,
	_CYAN,
	_BLACK
} color_n;

#define TOT_COLORS 4

#define PRINT_COLOR(number) switch (number) { \
				case _RED: \
					SWITCH_COLOR(RED)\
					printf("Red"); \
					break; \
				case _YELLOW: \
					SWITCH_COLOR(YELLOW)\
					printf("Yellow"); \
					break; \
				case _CYAN: \
					SWITCH_COLOR(CYAN)\
					printf("Cyan"); \
					break; \
				case _BLACK: \
					SWITCH_COLOR(BLACK)\
					printf("Black"); \
					break; \
				default: \
					break; \
} \

#define SWITCH_COLOR(c) switch (c) { \
				case RED: \
					printf("\033[1;31m");\
					break; \
				case YELLOW: \
					printf("\033[1;33m");\
					break; \
				case CYAN: \
					printf("\033[1;34m");\
					break; \
				case BLACK: \
					printf("\033[1;37m");\
					break; \
				default: \
					printf("\033[0m");\
					break; \
} \

typedef struct {
	int points;
	int colors[TOT_COLORS];
} potion_t;


void printPotion(potion_t p)
{
	int i = 0;
	printf("Potion made of:\n");
	for (i = 0; i < TOT_COLORS; i++) {
		if (p.colors[i] != 0) {
			PRINT_COLOR(i);
			printf(" %d\n",p.colors[i]);
			SWITCH_COLOR(0)
		}
	}
	printf("Total points: %d\n",p.points);
}

#endif
