#ifndef __CONSTANTS_HPP__
#define __CONSTANTS_HPP__

#define ROWS 8
#define COLS 5

#define RED 'R'
#define YELLOW 'Y'
#define CYAN 'C'
#define BLACK 'B'
#define EMPTY 'X'

#define HELP -2
#define INSTNAME "instructions.txt"
#define STATSNAME "stats.csv"
#define SEPARATOR "================================="
#define SEP1	  "=========="         //"=========="

#define PRETTY_PRINT(title) 	printf("%s\n",SEPARATOR); \
				printf("%s%s%s\n",SEP1,title,SEP1);\
				printf("%s\n",SEPARATOR);

#define MISSING_PARAMS "Missing parameters!"

#endif
